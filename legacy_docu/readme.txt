As of 02.2022 some of (partly very) old tex sources are removed from openCFS.
Here we colled the compiled pdfs. For tex sources dig in the cfs sources of
- share/doc/user
- share/doc/developer/develManual
- share/doc/auxilliaries
In compliance with https://cfs-dev.mdmt.tuwien.ac.at/cfs/CFS/-/issues/263

