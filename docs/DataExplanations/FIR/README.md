# Finite impulse response (FIR) filter

This filter is based on the theory of FIR based filters [@shenoi2006introduction]. Currently, this method is under development. For further details, the implementation status, and the application please contact  [Stefan Schoder](https://online.tugraz.at/tug_online/visitenkarte.show_vcard?pPersonenId=1D95C1A49837524D&pPersonenGruppe=3).


## Acknowledgement
Please provide an **acknowledgement** at the end of your publication using this software part for simulations

_The computational results presented have been achieved [in part] using the software openCFS [FIR]._

# References
\bibliography
