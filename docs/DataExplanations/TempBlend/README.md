# Temporal Blending

Temporal Blending is usually used to smoothly apply an external source to the PDE. Therefore the source data $s(t)$ is multiplied with a scalar blending function $f(t)$ to get the output

$r(t) = s(t) \cdot f(t)$

The blending function is defined as explained in [Math Expressions](../../Tutorials/Features/mathexpressions.md)

The filter is defined as following in the CFSdat xml input:

For temporal Blending with the blending function $f(t)=1-t^2$

```
<temporalBlending inputFilterIds="input" id="blend">
  <blendingFunction temporalFunction="1.0-t^2"/>
  <singleResult>
	<inputQuantity resultName="..."/>
	<outputQuantity resultName="..."/>
  </singleResult>
</temporalBlending>
```

---

## Acknowledgement
Please provide an **acknowledgement** at the end of your publication using this software part for simulations

_The computational results presented have been achieved [in part] using the software openCFS [Temporal Blending]._

---