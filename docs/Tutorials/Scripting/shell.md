# Scripting with the commando shell

Scripting via commando shell can be done in every operation system (Windows, Mac, Linux, etc...). In this tutorial we will focus on script for linux, however shell-scripts for other operation systems follow the same principle (there can be syntex-differences).

Here is a basic script with evaluates the deformation of a cantilever on different planets:
```
#!/bin/bash -i

#Loop through the planets
for planet in 'Moon' 'Earth' 'Mars' 'Jupiter';
do
  #Set default gravity value
  gravity='1'
  
  #Print out on which planet we are
  echo Simulationon $planet
  
  #Set gravity according to planet
  if [ $planet = 'Moon' ]; then
    gravity='1.62'
  elif [ $planet = 'Earth' ]; then
    gravity='9.81'
  elif [ $planet = 'Mars' ];then
    gravity='3.72'
  elif [ $planet = 'Jupiter' ]; then
    gravity='24.79'
  fi
  
  #Print out the used gravity
  echo with gravity $gravity m/s^2
  
  #Create Simulation-name
  job=simulation_$planet

  #Search in template-file for GRAVITY and replace it with $gravity and save it under $job'.xml'
  sed 's/GRAVITY/'$gravity'/g' simulation_template.xml > $job'.xml'
    
  #Simulate with openCFS
  #if unsure where cfs is located use the command "type cfs"
  /path/to/cfs $job
done
```

**Be caution with spaces in shell-scripts! Its a syntax error!**

General convention is that shell-scripts are saved with a `.sh` ending, like `script.sh`.

# Running the script

Run the script in the terminal either via the `bash`-command 


```
$bash script.sh
```

or execute it directly with

```
$./script.sh
```

but be aware that in this case you must made the script executable with

```
chmod +x script.sh
```

# Example and Tutorial Suggestion

Here is a [small example for downloading](shell.zip). Just unzip it and run the script in the terminal command and see what happens.

Try to:

* Add more planets
* Modify the placeholder string `GRAVITY` to `G` and ajust the template and the script accordingly
* Hint: Be careful with spaces, use the exact syntax as shown
