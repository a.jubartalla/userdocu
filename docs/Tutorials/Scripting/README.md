In this section we will show you, how you can use scripts to automate your simulation-workflow. This especially useful for 

* optimization study
* convergence study
* parameter study
* and many more

 where a lot of similar simulations are performed, where only small details like mesh-size, materials or loads are changeing.
 In those cases it is best to use script to reduce the repetive work.
 
 In the following we will show you some examples with:
 
 * [Shell-Scripts](shell.md)
 * [Phyton-Scripts](python.md)
