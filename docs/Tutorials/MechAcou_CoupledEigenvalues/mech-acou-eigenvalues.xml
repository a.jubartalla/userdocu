<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://www.cfs++.org/simulation 
https://opencfs.gitlab.io/cfs/xml/CFS-Simulation/CFS.xsd">

    <!-- define which files are needed for simulation input & output-->
    <fileFormats>
        <input>
            <cdb fileName="pipe.cdb"/>
        </input>
        <output>
            <hdf5/>
        </output>
        <materialData file="mat.xml" format="xml"/>
    </fileFormats>

    <!-- material assignment -->
    <domain geometryType="axi">
        <regionList>
            <region name="S_a" material="water"/>
            <region name="S_s" material="rubber"></region>
        </regionList>
    </domain>
    
    <!-- acoustic EVs -->
    <sequenceStep index="1">
        <analysis>
            <eigenFrequency>
                <isQuadratic>no</isQuadratic>
                <numModes>10</numModes>
                <freqShift>0</freqShift>
                <writeModes>yes</writeModes>
            </eigenFrequency>
        </analysis>
        <pdeList>
            <acoustic formulation="acouPressure" pdeFormulation="default" feSpaceFormulation="default">
                <regionList>
                    <region name="S_a"/>
                </regionList>
                <storeResults>
                    <!-- add required outputs -->
                    <nodeResult type="acouPressure">
                        <allRegions/>
                    </nodeResult>
                </storeResults>
            </acoustic>
        </pdeList>
    </sequenceStep>
    
    <!-- mechanic EVs -->
    <sequenceStep index="2">
        <analysis>
            <eigenFrequency>
                <isQuadratic>no</isQuadratic>
                <numModes>10</numModes>
                <freqShift>0</freqShift>
                <writeModes>yes</writeModes>
            </eigenFrequency>
        </analysis>
        <pdeList>
            <mechanic subType="axi">
                <regionList>
                    <region name="S_s"/>
                </regionList>
                <bcsAndLoads>
                    <fix name="L_s_end">
                        <!-- the z-component is used to refer to the axial component in an axially symmetrix setup -->
                        <comp dof="z"/>
                    </fix>
                </bcsAndLoads>
                <storeResults>
                    <nodeResult type="mechDisplacement">
                        <allRegions/>
                    </nodeResult>
                </storeResults>
            </mechanic>
        </pdeList>
    </sequenceStep>
    
    <!-- coupled EVs -->
    <sequenceStep index="3">
        <analysis>
            <eigenFrequency>
                <isQuadratic>no</isQuadratic>
                <!-- the FEAST eigenvalue solver uses contour integration
                to find all EIGENVALUES (not frequencies!) in an interval -->
                <minVal>100</minVal>
                <maxVal>3e+6</maxVal>
                <writeModes>yes</writeModes>
            </eigenFrequency>
        </analysis>
        <pdeList>
            <acoustic formulation="acouPressure">
                <regionList>
                    <region name="S_a"/>
                </regionList>
                <storeResults>
                    <!-- add required outputs -->
                    <nodeResult type="acouPressure">
                        <allRegions/>
                    </nodeResult>
                </storeResults>
            </acoustic>
            <mechanic subType="axi">
                <regionList>
                    <region name="S_s"/>
                </regionList>
                <bcsAndLoads>
                    <fix name="L_s_end">
                        <comp dof="z"/>
                    </fix>
                </bcsAndLoads>
                <storeResults>
                    <nodeResult type="mechDisplacement">
                        <allRegions/>
                    </nodeResult>
                </storeResults>
            </mechanic>
        </pdeList>
        <couplingList>
            <direct>
                <acouMechDirect>
                    <surfRegionList>
                        <surfRegion name="L_as"/>
                    </surfRegionList>
                </acouMechDirect>
            </direct>
        </couplingList>
        <linearSystems>
            <system>
                <solutionStrategy>
                    <standard>
                        <!-- use non-symmetric matrix storage -->
                        <matrix storage="sparseNonSym"/>
                    </standard>
                </solutionStrategy>
                <eigenSolverList>
                    <!-- activate the FEAST eigenvalue solver -->
                    <feast>
                       <logging>true</logging>
                        <m0>20</m0>
                        <stopCrit>9</stopCrit>
                    </feast>
                </eigenSolverList>
            </system>
        </linearSystems>
    </sequenceStep>
    
</cfsSimulation>
