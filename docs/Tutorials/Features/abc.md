# Absorbing Boundary Condition (ABC)

Similar to a Perfectly Matched Layer (PML) an ABC is used for the treatment of open domain problems. Taking a look at the acoustic PDE for example, the soundhard as well as the soundsoft boundary condition completely reflect impinging waves, hence, special boundary conditions need to be used to deal with problems where the sound should be radiated freely outwards of the computational domain. 

## Theoretical background based on the acoustic PDE 

If an orthogonlly impinging wave is expected (e.g. in a channel), an ABC will be sufficient and more efficient regarding the simulation time, since no additional elements are required. If one treats a more general problem, where not only orthognally impinging waves are expected, a PML will be the better solution (although additional elements are required which will inherently cause a larger system matrix as well as longer computational time). The main idea is to construct a boundary condition such that the wave can only pass in one direction. For the more general 3D case this can be done by imposing the following boundary condition:
\begin{equation}
\frac{\partial p_{\textrm a}}{\partial \mathbf n} = -\frac{1}{c} \frac{\partial p_{\textrm a }}{\partial t}
\end{equation}
It has to be noted that this boundary condition only works ideal (no reflections) in the case of an orthogonally impinging wave, which is usually not the case.

## Usage of the ABC

In order to define the ABC in openCFS one has to define a surface used for the ABC and the adjoining volume region as follows:
```
<bcsAndLoads>
	<absorbingBCs name="Surface_ABC" volumeRegion="Volume_ABC"/>
</bcsAndLoads>
```

## Important things to consider when using an ABC

Regarding the usage of an ABC the following points have to be considered:

* Works well for orthogonally impinging waves, for a more general field use a PML instead.
* More efficient (computational time, system matrix, etc.) than a PML since no additional elements are required.

## Examples

Here you can find links to exemplary applications which use the concept of an ABC:


