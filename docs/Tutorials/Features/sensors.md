# Results at nodes/elements

In CFS it is possible to monitor results at specific nodes and elements. They are defined in the `domain` section of the input-xml. The coordinate location that needs to be monitored can be specified using `nodeList` 
```
<domain>
  <nodeList>
    <!-- refer to 'name' in the output definition -->
    <nodes name="node_name">
      <coord x="0.0" y="0.0" z="1.0"/>
    </nodes>
  </nodeList>
</domain>
```
for a nodal result at these coordinates or `elemList` for an element result. CFS selects the closest entity to the coordinate location specified.
The defined nodes and elements are then referred to under `storeResults`in the `sequenceStep` section.
```
<storeResults>
  <nodeResult type="mechDisplacement">
    <allRegions/>
    <nodeList>
      <nodes name="node_name" outputIds="txt"/>
    </nodeList>
  </nodeResult> 
  <elemResult type="mechStress">
    <allRegions/>
    <elemList>
      <elems name="element_name"/>
    </elemList>  
  </elemResult>
</storeResults>
```
This way, the nodal and element groups are also written to the results file and can be accessed in ParaView. Additionally, if `outputIds="txt"` is specified and the text output is enabled in the `fileFormats` section (see also below under *Results at arbitrary locations*), the results are saved as a text file.

Furthermore, a list of nodes/elements can be specified:
```
<nodes name="nodelist_name">
  <list>
    <freeCoord comp="y" stop="L/4" inc="0.05" start="0"/>
    <fixedCoord comp="x" value="0"/>
  </list>
</nodes>
```
With this definition, a line of points is created along the y-axis from 0 to L/4 with the given increment and `x=0`. CFS then again selects the closest nodes (or elements, if a list of elements is defined) to these points which then can be utilized equivalently in `storeResults`.


# Results at arbitrary locations

When results at arbitrary points, i.e. not at nodes or on an element level, are required, `sensorArray` can be used. In this case, the shape functions are utilized to evaluate the solution at the given location. A prerequisite of using this feature is the enabling of the text output in the `fileformats` section:
```
<fileFormats>
  <output>
    <hdf5/>
    <text/>
  </output>
</fileFormats>
```
The points can be defined either in an external file
```
<sensorArray type="acouPressure" fileName="basename">
  <coordinateFile fileName="circle.csv"/>
</sensorArray>
```
where the `x`,`y` and `z` coordinates have to be provided in the coordinate file, one line per point, or in the input-xml 
```
<sensorArray type="acouPressure" fileName="basename">
  <parametric>
    <list comp="x" start="0" stop="0" inc="0"/>
    <list comp="y" start="-L" stop="L" inc="0.01"/>
  </parametric>
</sensorArray>
```
In this case, sensor points are created along a line at `x=0` from -L to L in `y` with the given increment. The saved output files are named `basename-N` where `N` is the number of the time/frequency step. The columns in the files are `N_e, x, y, z, Re(v), Im(v), x_e, y_e, z_e`, i.e. element id, global coordinates, value (real and imaginary part), local coordinates. Depending on the dimension and result type (real/complex), the according columns are omitted.
